# Android Developer Test #

## Task ##
The aim of this task is to assess the understanding of Android and Java
development by building an interactive organisation chart of the company. 

App type: A master / detail style application that runs on Android phone and Tablet

## Criteria ##

* Render a list of all Mubaloo team members grouped by team, consuming the JSON feed provided.
* Highlight the team leads visually in order to distinguish them from other team members.
* Show a detail view of each team member in a new view when tapped.
* Store the provided data in a SQLite database so it can be used offline.
* Unit tests on critical components such as the web service and database.
* Relevant documentation